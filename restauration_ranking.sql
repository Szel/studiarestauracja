-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2020 at 04:39 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `restauration_ranking`
--

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `Id` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`Id`, `Name`) VALUES
(1, 'Bydgoszcz'),
(2, 'Toruń');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(150) NOT NULL,
  `FileName` varchar(50) NOT NULL,
  `RestaurantId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`Id`, `Name`, `Description`, `FileName`, `RestaurantId`) VALUES
(5, 'abc', 'abc', '158022480020190803_135210.jpg', 2),
(6, 'CC', 'CC', '158022481620190803_181033.jpg', 2),
(7, 'abc2', 'abc2', '158022585320190803_135210.jpg', 5);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `Id` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`Id`, `Name`) VALUES
(1, 'Nie zbliżać się!'),
(2, 'Należy coś zmienić'),
(3, 'Neutralna'),
(4, 'Dobra jakość'),
(5, 'Najlepszy wybór');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant`
--

CREATE TABLE `restaurant` (
  `Id` int(11) NOT NULL,
  `Name` varchar(120) NOT NULL,
  `Description` text NOT NULL,
  `Address` varchar(200) NOT NULL,
  `PhoneNumber` varchar(16) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Website` varchar(50) NOT NULL,
  `CityId` int(11) NOT NULL,
  `RatingId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `restaurant`
--

INSERT INTO `restaurant` (`Id`, `Name`, `Description`, `Address`, `PhoneNumber`, `Email`, `Website`, `CityId`, `RatingId`) VALUES
(2, 'AA', 'BB', 'cc', '234', 'em', 'str', 1, 1),
(3, 'AA', 'BB', 'cc', '2345', 'em', 'str', 2, 5),
(4, 'Costam', 'opis', 'add', 'u27487', 'email', 'kdfjkd', 1, 1),
(5, 'BA', 'kdjf', 'kdsjf', '23423', 'emadjf@dfmkdsa.sd', 'fgfg', 1, 4);

-- --------------------------------------------------------

--
-- Stand-in structure for view `restaurantdetails`
-- (See below for the actual view)
--
CREATE TABLE `restaurantdetails` (
`Id` int(11)
,`Name` varchar(120)
,`Description` text
,`Address` varchar(200)
,`PhoneNumber` varchar(16)
,`Email` varchar(50)
,`Website` varchar(50)
,`CityId` int(11)
,`RatingId` int(11)
,`RatingName` varchar(20)
,`CityName` varchar(200)
);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `Id` int(11) NOT NULL,
  `FullName` varchar(50) NOT NULL,
  `Comment` text NOT NULL,
  `RestaurantId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`Id`, `FullName`, `Comment`, `RestaurantId`) VALUES
(1, 'Marian Kowalski', 'Nie podoba mi się', 2),
(2, 'Zbigniew Kowalski', 'A mi tak', 2);

-- --------------------------------------------------------

--
-- Structure for view `restaurantdetails`
--
DROP TABLE IF EXISTS `restaurantdetails`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `restaurantdetails`  AS  select `restaurant`.`Id` AS `Id`,`restaurant`.`Name` AS `Name`,`restaurant`.`Description` AS `Description`,`restaurant`.`Address` AS `Address`,`restaurant`.`PhoneNumber` AS `PhoneNumber`,`restaurant`.`Email` AS `Email`,`restaurant`.`Website` AS `Website`,`restaurant`.`CityId` AS `CityId`,`restaurant`.`RatingId` AS `RatingId`,`rating`.`Name` AS `RatingName`,`city`.`Name` AS `CityName` from ((`restaurant` left join `city` on(`city`.`Id` = `restaurant`.`CityId`)) left join `rating` on(`rating`.`Id` = `restaurant`.`RatingId`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ImageRestaurant` (`RestaurantId`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CityId` (`CityId`),
  ADD KEY `RestaruantRating` (`RatingId`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ReviewRestaurant` (`RestaurantId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `restaurant`
--
ALTER TABLE `restaurant`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `image`
--
ALTER TABLE `image`
  ADD CONSTRAINT `ImageRestaurant` FOREIGN KEY (`RestaurantId`) REFERENCES `restaurant` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `restaurant`
--
ALTER TABLE `restaurant`
  ADD CONSTRAINT `RestaruantRating` FOREIGN KEY (`RatingId`) REFERENCES `rating` (`Id`),
  ADD CONSTRAINT `RestaurantCity` FOREIGN KEY (`CityId`) REFERENCES `city` (`Id`);

--
-- Constraints for table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `ReviewRestaurant` FOREIGN KEY (`RestaurantId`) REFERENCES `restaurant` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
