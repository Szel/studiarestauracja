<?php
require_once('php/database.php');
$database = new Database();

$restaurants = [];
if(isset($_GET['search']) && !empty($_GET['search']))
{
    $search = $_GET['search'];
    $where['Name'] = ' like \'%'.$search.'%\'';
    $where['Description'] = ' like \'%'.$search.'%\'';
    $restaurants = $database->getRows('restaurantdetails', '*', $where, 'OR');
}else{
    $restaurants = $database->getRows('restaurantdetails');
}



?>
<?php include('header.php'); ?>
<?php 
    if(count($restaurants) == 0) echo 'Nie znaleziono restauracji';
    foreach ($restaurants as $restaurant) { ?>
    <div class="row restaurant-list-item">
        <div class="col">
            <h3><?php echo $restaurant['Name'] ?></h3>
            <div>
                Adres: <?php echo $restaurant['Address'].' '.$restaurant['CityName'].' | Tel.'.$restaurant['PhoneNumber'] ?>
            </div>
            <div>Ocena: <?php echo $restaurant['RatingName'] ?></div>
        </div>
        <div class="col-md-2 col-sm-12 d-flex align-items-center">
            <a href="details.php?id=<?php echo $restaurant['Id'] ?>" class="btn btn-primary">Szczegóły</a>
        </div>
    </div>
<?php } ?>

<!-- 
<div class="row restaurant-list-item">
    <div class="col">
        <h3>Nazwa restauracji</h3>
        <div>Adres: Bagińskiego 10, 83-202 Toruń | Tel. 123 332 124</div>
        <div>Ocena: Doskonałe</div>
    </div>
    <div class="col-md-2 col-sm-12 d-flex align-items-center">
        <a href="details.php" class="btn btn-primary">Szczegóły</a>
    </div>
</div> -->
<?php include('footer.php'); ?>