<?php
require_once('php/database.php');
$database = new Database();
$cities = $database->getRows("City");
$ratings = $database->getRows("Rating");
$errors = [];

$restaruant = [
    'Name' => '',
    'Name' => '',
    'Description' => '',
    'Address' => '',
    'PhoneNumber' => '',
    'Email' => '',
    'Website' => '',
    'CityId' => '',
    'RatingId' => '',
    'RatingName' => '',
    'CityName' => ''
];

$action = $_GET['action'];
if ($action === 'create' && isset($_POST['save'])) {
    // save

} else if ($action === 'edit') {
    $id = $_GET['id'];
    $where_restaurant['Id'] = '=' . $id;
    $restaruant = $database->getRow("restaurant", "*", $where_restaurant);
}

?>
<?php include('header.php'); ?>
<div class="row mt-4 mb-5">
    <div class="col">
        <form method="post" action="php/process.php">
            <input type="hidden" name="action" value="<?php echo $action ?>">
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
            <div class="form-group">
                <label for="formGroupExampleInput">Nazwa restauracji</label>
                <input type="text" class="form-control" name="name" value="<?php echo $restaruant['Name'] ?>">
            </div>
            <div class="form-group">
                <label>Opis</label>
                <textarea class="form-control" name="description"><?php echo $restaruant['Description'] ?></textarea>
            </div>
            <div class="form-group">
                <label>Adres</label>
                <input type="text" class="form-control" name="address" value="<?php echo $restaruant['Address'] ?>">
            </div>
            <div class="form-group">
                <label>Miejscowość</label>
                <select class="form-control" name="city">
                    <?php foreach ($cities as $city) { ?>
                        <option value="<?php echo $city['Id'] ?>" <?php if ($city['Id'] == $restaruant['CityId']) {
                                                                        echo 'selected';
                                                                    } ?>>
                            <?php echo $city['Name'] ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Nr telefonu</label>
                <input type="text" class="form-control" name="phone-number" value="<?php echo $restaruant['PhoneNumber'] ?>">
            </div>
            <div class="form-group">
                <label>Adres email</label>
                <input type="text" class="form-control" name="email" value="<?php echo $restaruant['Email'] ?>">
            </div>
            <div class="form-group">
                <label>Strona internetowa</label>
                <input type="text" class="form-control" name="website" value="<?php echo $restaruant['Website'] ?>">
            </div>
            <div class="form-group">
                <label>Ocena</label>
                <select type="text" class="form-control" name="rating">
                    <?php foreach ($ratings as $rating) { ?>
                        <option value="<?php echo $rating['Id'] ?>" <?php if ($rating['Id'] == $restaruant['RatingId']) {
                                                                        echo 'selected';
                                                                    } ?>>
                            <?php echo $rating['Name'] ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <button type="submit" name="save" class="btn btn-primary">Zapisz</button>
        </form>
    </div>
    <?php if ($action == 'edit') { ?>
        <div class="col">
            <form action="php/process.php" method="post" enctype="multipart/form-data">
                <input type="hidden" name="action" value="addImage">
                <input type="hidden" name="restaurantId" value="<?php echo $_GET['id'] ?>">
                <h4>Dodaj zdjęcie</h4>
                <div class="form-group">
                    <label for="formGroupExampleInput">Nazwa</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput">Opis</label>
                    <input type="text" class="form-control" name="description">
                </div>
                <div class="form-group">
                    <input type="file" class="form-control" name="images[0]">
                </div>
                <button type="submit" name="save" class="btn btn-primary">Dodaj obraz</button>
            </form>
        </div>
    <?php } ?>
</div>
<?php include('footer.php'); ?>