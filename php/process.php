<?php
require_once('database.php');
$database = new Database();
$uploadPath = '../static/uploads/';

if (!isset($_POST['action']) || empty($_POST["action"])) {
    die();
}

switch ($_POST['action']) {
    case 'addComment':{
        $restaurantId = strip_tags($_POST['restaurantId']);
        $fullname = strip_tags($_POST['fullname']);
        $comment = strip_tags($_POST['comment']);

        $data = array(
            "Id" => null,
            "FullName" => $fullname,
            "Comment" => $comment,
            "RestaurantId" => $restaurantId,
        );

        $database->insertRows('review', $data);
        
        header('Location: /details.php?id=' . $restaurantId);
        }
    break;
    case 'addImage': {
            $restaurantId = strip_tags($_POST['restaurantId']);
            $name = strip_tags($_POST['name']);
            $description = strip_tags($_POST['name']);

            $files = $_FILES['images'];
            if ($files != null) {
                $file_count = count($files['name']);

                if ($files['error'][0] == 0) {
                    $allowed = array('jpg', 'jpeg');
                    $filename = $files['name'][0];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    if (in_array($ext, $allowed)) {
                        if ($files['size'][0] < 3072000) {
                            $curr_time = time();
                            $filename = $curr_time . $filename;
                            $newname = $uploadPath . $filename;
                            if (!file_exists($newname)) {
                                if (move_uploaded_file($files['tmp_name'][0], $newname)) {
                                    $data = array(
                                        "Id" => null,
                                        "FileName" => $filename,
                                        "RestaurantId" => $restaurantId,
                                        "Name" => $name,
                                        "Description" => $description
                                    );
                                    $database->insertRows("Image", $data);
                                }
                            } else {
                                echo 'Błąd! Plik o podanej nazwie już istnieje';
                            }
                        } else {
                            echo 'Błąd! Plik jest za duży';
                        }
                    } else {
                        echo 'Błąd! Nieprawidłowy format obrazka. Dozwolone rozszerzenia to .jpeg, .jpg';
                    }
                } else {
                    echo 'Wystąpił błąd';
                }
            }

            header('Location: /edit.php?action=edit&id=' . $restaurantId);
        }
        break;
    case 'removeImage': {
        
        header('Location: /edit.php?id=' . $restaurantId);
        }
        break;
    case 'create': {
            $name = strip_tags($_POST['name']);
            $description = strip_tags($_POST['description']);
            $address = strip_tags($_POST['address']);
            $city = strip_tags($_POST['city']);
            $phoneNumber = strip_tags($_POST['phone-number']);
            $email = strip_tags($_POST['email']);
            $website = strip_tags($_POST['website']);
            $rating = strip_tags($_POST['rating']);

            $data = array(
                "Id" => null,
                "Name" => $name,
                "Description" => $description,
                "Address" => $address,
                "CityId" => $city,
                "PhoneNumber" => $phoneNumber,
                "Email" => $email,
                "Website" => $website,
                "RatingId" => $rating,
            );

            $database->insertRows('Restaurant', $data);
            $restaurantId = $database->getLastInsertedId();
            
            header('Location: /details.php?id=' . $restaurantId);
        }
        break;

    case 'edit': {
            $id = strip_tags($_POST['id']);
            $name = strip_tags($_POST['name']);
            $description = strip_tags($_POST['description']);
            $address = strip_tags($_POST['address']);
            $city = strip_tags($_POST['city']);
            $phoneNumber = strip_tags($_POST['phone-number']);
            $email = strip_tags($_POST['email']);
            $website = strip_tags($_POST['website']);
            $rating = strip_tags($_POST['rating']);

            $data = array(
                "Id" => $id,
                "Name" => $name,
                "Description" => $description,
                "Address" => $address,
                "CityId" => $city,
                "PhoneNumber" => $phoneNumber,
                "Email" => $email,
                "Website" => $website,
                "RatingId" => $rating,
            );
            $where_hotel['Id'] = '=' . $id;
            $database->updateRows("Restaurant", $data, $where_hotel);
            header('Location: /details.php?id=' . $id);
        }
        break;
    case 'deletehotel': {
            $id = strip_tags($_POST['id']);
            $where_hotel['ID'] = '=' . $id;
            $database->removeRows("HOTEL", $where_hotel);
            header('Location: index.php');
        }
        break;
}
