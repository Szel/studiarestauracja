<?php
require_once('php/database.php');
$database = new Database();
$id = $_GET['id'];
$whereRestaurant['Id'] = '=' . $id;
$restaruant = $database->getRow("restaurantdetails", "*", $whereRestaurant);
$whereImage['RestaurantId'] = '=' . $id;
$images = $database->getRows("Image", "*", $whereImage);
$wherereview['RestaurantId'] = '=' . $id;
$reviews = $database->getRows("Review", "*", $wherereview);
?>
<?php include('header.php'); ?>
<?php if (count($images) == 0) { ?>
    <h1 style="text-align:center"><?php echo $restaruant["Name"] ?></h1>
<?php } else { ?>
    <div id="carouselExampleControls" class="carousel slide center" data-ride="carousel" style="max-width:600px; margin:auto">
        <div class="carousel-inner">
        <?php 
            $first = true;
            foreach ($images as $image) { ?>
            <div class="carousel-item <?php if($first){ $first = false; echo 'active';} ?>">
                <img src="static/uploads/<?php echo $image['FileName'] ?>" class="d-block w-100" alt="<?php echo $image['Description'] ?>">
            </div>
            <?php } ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <div style="position:absolute; bottom:0px; background-color: #00000060; width:100%">
            <h1 style="color:#ffffff; text-align:center"><?php echo $restaruant["Name"] ?></h1>
        </div>
    </div>
<?php } ?>
<div class="row mt-4 mb-5">
    <!-- Left -->
    <div class="col">
        <h3>Opis</h3>
        <p>
            <?php echo $restaruant["Description"] ?>
        </p>

        <h3>Komentarze</h3>
        <?php foreach ($reviews as $review) { ?>
                
            <div class="comment">
                <b><?php echo $review["FullName"] ?></b>
                <p>
                    <?php echo $review["Comment"] ?>
                </p>
            </div>
        <?php } ?>

        <h4>Dodaj komentarz</h4>
        <form method="post" action="php/process.php">
        
            <input type="hidden" name="action" value="addComment">
            <input type="hidden" name="restaurantId" value="<?php echo $_GET['id'] ?>">
            <div class="form-group">
                <input class="form-control" type="text" placeholder="Imię i Nazwisko" name="fullname"/>
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="4" placeholder="Opis" name="comment"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Dodaj</button>
        </form>
    </div>
    <!-- Right -->
    <div class="col-4">
        <h3>Dane kontaktowe</h3>
        <p><b>Adres</b> <?php echo $restaruant["Address"] ?>, <?php echo $restaruant["CityName"] ?></p>
        <p><b>Tel.</b> <a href="tel:234432134"><?php echo $restaruant["PhoneNumber"] ?></a></p>
        <p><b>Email</b> <a href="mailto:mc@mcdonalds.com"><?php echo $restaruant["Email"] ?></a></p>
        <p><b>www</b> <a href="<?php echo $restaruant["Website"] ?>"><?php echo $restaruant["Website"] ?></a></p>

        <h3>Ocena</h3>
        <p>
            <?php for ($i = 0; $i < $restaruant["RatingId"]; $i++) {
                echo '&#9733; ';
            } ?>
            <br>
            <?php echo $restaruant["RatingName"] ?>
        </p>

        <a class="btn btn-secondary btn-block" href="edit.php?action=edit&id=<?php echo $id ?>">Edycja</a>
    </div>

</div>
<?php include('footer.php'); ?>